import { nanoid } from 'https://deno.land/x/nanoid/mod.ts'
import { getFreePort } from 'https://deno.land/x/free_port@v1.2.0/mod.ts'
import { ensureFile } from 'https://deno.land/std@0.101.0/fs/mod.ts'
import {
  WebExtMessaging,
  sendToExtension,
} from './deno-web-ext-native-messaging.ts'
import type { ServerMessage, serverRequestType, JsonValue } from '../types.ts'
import { httpServerInit } from './http-interface.ts'
import createLogger from './logger.ts'

const homepath = Deno.env.get('HOME')
const appPort = await getFreePort(8000)

const log = await createLogger(
  `${homepath}/.local/var/log/cookie-monger/instance-${appPort}`
)

async function initiateRequest(reqType: serverRequestType, msg?: string) {
  const reqID = nanoid()
  log.debug(`Prepping request ${reqID}`)
  const m: ServerMessage = {
    text: msg || '',
    type: reqType,
    requestId: reqID,
  }
  await sendToExtension(JSON.stringify(m))
  log.debug(`Sent request ${reqID}`)
  return m
}

const globalResponseMap: {
  [k: string]: { payload: JsonValue; error?: boolean }
} = {}

async function getResponseFromExtension(
  reqType: serverRequestType,
  messageText?: string
): Promise<JsonValue> {
  // maybe use effector here instead? https://deno.land/x/effector@effector%4021.8.1
  const req = await initiateRequest(reqType, messageText)
  log.info(`Initiated request ${req.requestId}`)
  await new Promise((resolve, reject) => {
    const timeout = setTimeout(
      () => reject('timeout waiting for extension response'),
      7000
    )
    addEventListener(`full-response-${req.requestId}`, (_event) => {
      log.info(`Received ${req.requestId} response`)
      clearTimeout(timeout)
      resolve(undefined)
    })
    addEventListener(`error-response-${req.requestId}`, (_event) => {
      log.error(`Received ${req.requestId} error`)
      clearTimeout(timeout)
      reject(new Error('timeout'))
    })
  })

  const response = globalResponseMap[req.requestId]
  if (response.error) {
    throw new Error(String(response.payload))
  }
  return response.payload
}

type portMap = { [k: string]: number }
export const updateJsonFile = async (
  fpath: string,
  cb: (a: portMap) => portMap
) => {
  await ensureFile(fpath)
  const currentMapStr = await Deno.readTextFile(fpath)
  const currentMap = currentMapStr.length > 0 ? JSON.parse(currentMapStr) : {}
  const updatedContents = await Promise.resolve(cb(currentMap))
  await Deno.writeTextFile(fpath, JSON.stringify(updatedContents))
}

const init = async () => {
  log.info('Starting native host')
  addEventListener('unhandledrejection', (err) => {
    log.error(err)
  })
  const serverController = httpServerInit(getResponseFromExtension, appPort)

  const responsePrepArea: { [k: string]: string } = {}
  const moveResponseToGlobal = (reqId: string, error?: boolean) => {
    let finalPayload = responsePrepArea[reqId] ?? ''
    try {
      const finalPayloadTmp = JSON.parse(finalPayload)
      finalPayload = finalPayloadTmp
    } catch (err) {
      log.error(err)
    } finally {
      globalResponseMap[reqId] = { payload: finalPayload, error }
    }
  }

  log.info('Starting stdin reader')
  const readerPromise = WebExtMessaging((msg: string) => {
    const incoming = JSON.parse(msg)
    responsePrepArea[incoming.requestId] =
      responsePrepArea[incoming.requestId] ?? ''
    if (incoming.error) {
      log.error('received an errror!')
      moveResponseToGlobal(incoming.requestId, true)
      dispatchEvent(new Event(`error-response-${incoming.requestId}`))
    }
    responsePrepArea[incoming.requestId] += incoming.text
    if (!incoming.moreIncoming) {
      moveResponseToGlobal(incoming.requestId)
      dispatchEvent(new Event(`full-response-${incoming.requestId}`))
    }
  })

  log.info('Looking up client id')
  setTimeout(() => {
    getResponseFromExtension('whoami').then((resp) => {
      updateJsonFile(
        `${homepath}/.cookie-monger-profile-port-map`,
        (portMapCurrent) => {
          // filter other entries that claim to use our port
          const portMap = Object.fromEntries(
            Object.entries(portMapCurrent).filter(([_, v]) => v !== appPort)
          )
          return { ...portMap, [String(resp)]: appPort }
        }
      )
    })
  }, 200) // ensure we're ready to read the response

  log.error('Application starting')

  await readerPromise
  serverController.abort()
}

if (import.meta.main) await init()
