import { Application, Router, helpers } from 'https://deno.land/x/oak/mod.ts'
import type { serverRequestType, JsonValue } from '../types.ts'

type extSender = (reqType: serverRequestType, messageText?: string) => Promise<JsonValue>

const pingExtension = (getResponseFromExtension: extSender) =>
    async function(ctx: any) {
        const res = await getResponseFromExtension('ping', '')
        ctx.response.body = res
    }

const handleCookiesReq = (getResponseFromExtension: extSender) =>
    async function(ctx: any) {
        const { domain } = helpers.getQuery(ctx)
        if (domain === undefined) {
            ctx.throw(404, 'No cookies found for domain')
            return
        }
        const res = await getResponseFromExtension('cookieFetch', domain)
        ctx.response.body = JSON.stringify(res)
    }

export const httpServerInit = (sendToExt: extSender, port: number) => {
    const app = new Application()

    const router: Router = new Router()
    router.get('/ready', async (ctx) => {
        ctx.response.body = 'All good!'
    })
    router.get('/ping', pingExtension(sendToExt))
    router.get('/cookies', handleCookiesReq(sendToExt))
    app.use(router.routes())
    app.use(router.allowedMethods())

    const controller = new AbortController()
    const { signal } = controller
    app.listen({ port, signal })

    return controller
}
