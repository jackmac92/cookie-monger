import { Buffer } from 'https://deno.land/std@0.101.0/node/buffer.ts'
import { writeAll } from 'https://deno.land/std@0.101.0/io/util.ts'

export async function sendToExtension(msg: string) {
  const len = Buffer.alloc(4)
  const buf = Buffer.from(msg)
  len.writeUInt32LE(buf.length, 0)
  const output = Buffer.concat([len, buf])

  await writeAll(Deno.stdout, output)
}

export async function WebExtMessaging(
  handler: (a: string) => void | Promise<void>
) {
  while (true) {
    const buf = new Uint8Array(4)
    const n = await Deno.stdin.read(buf)
    if (n === null) {
      // EOF
      break
    }
    const length = new DataView(buf.buffer).getInt32(0, true)
    const valueBuf = new Uint8Array(length)

    const n2 = await Deno.stdin.read(valueBuf)
    if (n2 === null) {
      // EOF
      break
    }
    const value = new TextDecoder().decode(valueBuf.subarray(0, n2))
    if (value.length) {
      await Promise.resolve(handler(value))
    }
  }
}
