export const allowedOrigins: string[] = [
    'chrome-extension://alibnlegnkphjgbmmplmmjdjpllgmmae/',
]
export const description: string = 'Really cool thingamabobber'
export const resourceId: string = 'com.jackhoff_industriez.cookie_monger'
export const denoFlags: string = '-A --unstable'
