import * as log from 'https://deno.land/std@0.93.0/log/mod.ts'
import { dirname } from 'https://deno.land/std@0.110.0/path/mod.ts'
import { ensureDirSafe } from 'https://gitlab.com/jackmac92/ensurePathExists/-/raw/master/mod.ts'

export default async function (logFile: string) {
  await ensureDirSafe(dirname(logFile))
  await log.setup({
    // seems to use the maximum provided level (e.g. no debug messages unless both are set to debug)
    handlers: {
      file: new log.handlers.FileHandler('DEBUG', {
        filename: logFile,
        // you can change format of output message using any keys in `LogRecord`.
        formatter: '{levelName} {msg}',
      }),
    },

    loggers: {
      default: {
        level: 'DEBUG', // defaults to INFO
        handlers: ['file'],
      },
    },
  })

  return log.getLogger()
}
