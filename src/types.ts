interface MessageBase {
  type: string
  text: string
  requestId: string
}

const _serverRequestType = ['ping', 'cookieFetch', 'whoami'] as const

export type serverRequestType = typeof _serverRequestType[number]

export interface ServerMessage extends MessageBase {
  type: serverRequestType
}

export interface ClientMessage extends MessageBase {
  type: 'response'
  moreIncoming: boolean
  extensionInstallId: string
  error?: string
}

export type JsonArray = Array<JsonValue>

export type JsonValue =
  | string
  | number
  | boolean
  | null
  | JsonObject
  | JsonArray

export type JsonObject = { [Key in string]?: JsonValue }
