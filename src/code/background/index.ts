import { browser } from 'webextension-polyfill'
import type { Permissions } from '@types/webextension-polyfill'
import {
  createContextMenu,
  ensureInstallId,
  getCookiesFromDomain,
  getLocalStorage,
  pushToLocalList,
  textNotification,
  getInstallId,
} from 'browser-ext-utilz'
import { ClientMessage } from '../../types'

let extensionInstallId: string

setTimeout(() => {
  ensureInstallId()
    .then((z) => {
      extensionInstallId = z
    })
    .catch((err) => console.error('ensure install id failed!!', err))
}, 1000)

const port = browser.runtime.connectNative(
  'com.jackhoff_industriez.cookie_monger'
)

const isAllowedToReadCookiesForDomain = async (
  domain: string
): Promise<boolean> => {
  const perms = {
    origins: [`https://${domain}/`, `http://${domain}/`],
  }
  const alreadyHasPermission = await browser.permissions.contains(perms)
  if (alreadyHasPermission) {
    return true
  }

  try {
    const userApprovedNewPermission = await new Promise((resolve, _reject) => {
      textNotification(
        `Allow reading cookies from ${domain}?`,
        'Click to allow access',
        () => {
          resolve(browser.permissions.request(perms))
          return Promise.resolve()
        }
      )
    })
    return userApprovedNewPermission as boolean
  } catch (err) {
    new Array(10).forEach(() => {
      console.log(
        'TODO verify that this only errors out when the user is not available'
      )
    })
    console.error(err)
    await pushToLocalList('request_perms_when_ready', perms)
  }

  return false
}

const handleNativeRequest = async (
  reqType: string,
  reqArgs: string
): Promise<any> => {
  console.debug('Routing native host message', reqType, reqArgs)
  switch (reqType) {
    case 'ping':
      return Promise.resolve('pong')
    case 'whoami':
      return getInstallId()
    case 'cookieFetch': {
      const domain = reqArgs
      return isAllowedToReadCookiesForDomain(domain).then((hasAccess) => {
        if (!hasAccess) {
          console.error('FAILED NOPERM cookieFetch', domain)
          return Promise.reject(
            new Error('Not allowed to read from that domain')
          )
        }
        console.debug('Getting cookies for domain', domain)
        return getCookiesFromDomain(domain)
      })
    }
    case 'responseOfSize': {
      const size = parseInt(reqArgs, 10)
      const result = 'dummyText...'.repeat(size)
      return Promise.resolve(result)
    }
    default: {
      return Promise.reject(`Unknown type ${reqType}`)
    }
  }
}

port.onMessage.addListener((msg) => {
  console.debug('Received message from native host', msg)
  const { requestId, type, text } = msg
  port.postMessage({ willRespond: true, extensionInstallId })
  handleNativeRequest(type, text)
    .then(
      (result) => ({ text: JSON.stringify(result) }),
      (err) => ({ text: err, isError: true })
    )
    .then(
      async ({ isError, text = '' }: { isError?: boolean; text: string }) => {
        const textKey = isError ? 'error' : 'text'
        const respMessage = {
          [textKey]: text,
          requestId,
          type: 'response',
          moreIncoming: true,
          extensionInstallId,
        } as ClientMessage
        if (!(text.length > 1)) {
          console.warn('No text parameter on response, bailing', respMessage)
          return
        }
        console.debug('Responding with message of length', text.length)
        const textChunks = text.match(/.{1,888}/g) ?? []
        await textChunks.reduce(
          (prom, responseChunk, i) =>
            prom.then((_) => {
              console.debug(
                'Sending chunk (n) (length)',
                i,
                responseChunk.length
              )
              port.postMessage({ ...respMessage, [textKey]: responseChunk })
              return new Promise((r) => setTimeout(r, 222))
            }),
          Promise.resolve()
        )

        port.postMessage({ ...respMessage, text: '', moreIncoming: false })
        console.debug('Finished sending message', text)
      }
    )
})

port.onDisconnect.addListener((something) => {
  // TODO check error message string for issues which indicate host setup is not present

  setTimeout(() => {
    // HACK to prevent this running during the test
    textNotification(
      'You must install the native host complement to this extension!',
      'Click here for details',
      (_) =>
        browser.tabs
          .create({
            url: 'https://gitlab.com/jackmac92/cookie-monger/-/blob/master/README.md#installing-the-native-host',
          })
          .then(async () => {})
    )
  }, 30000)
  console.info('Disconnected from native host', something)
})

const allOrigins = ['https', 'http'].map((h) => `${h}://*/*`)

browser.permissions.contains({ origins: allOrigins }).then((hasPermissions) => {
  console.log('Value of hasPermissions is .... ', hasPermissions)
  if (!hasPermissions) {
    createContextMenu(
      'Grant permission to all hosts',
      (clearThisMenuOption) => {
        browser.permissions.request({ origins: allOrigins })
        clearThisMenuOption()
      }
    )
  }
})

getLocalStorage('request_perms_when_ready', [])
  .then((permList) => {
    const permsToAdd = permList as Permissions.Permissions[]
    if (permsToAdd.length === 0) return
    textNotification(
      'Provide previously requested permissions?',
      'Take a look at the optional permissions you requested?',
      async () => {
        for (const permission of permsToAdd) {
          await browser.permissions.request(permission)
        }
      }
    )
  })
  .catch((err) => {
    console.log('An error occurred checking for pending optional permissions')
    console.warn(err)
  })
