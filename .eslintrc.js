module.exports = {
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: { modules: true }
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      }
    }
  },
  extends: ['problems', 'plugin:prettier/recommended'],
  env: {
    browser: true,
    webextensions: true,
    es6: true,
    node: true
  },
  overrides: [
    {
      files: ['**/*.spec.ts'],
      parser: '@typescript-eslint/parser',
      env: { jest: true }
    },
    {
      files: ['**/*.ts', '**/*.tsx'],
      parser: '@typescript-eslint/parser'
    }
  ],
  rules: {
    strict: 0,
    'arrow-parens': 0,
    semi: ['error', 'never'],
    'no-global-assign': 1,
    'no-undef': 0,
    'no-unused-vars': ["error", { "argsIgnorePattern": "^_" }],
    quotes: ["error", "single", { "allowTemplateLiterals": true }],
    'no-restricted-globals': 'warn',
    'consistent-return': 0,
    'no-extra-semi': 0,
    'comma-dangle': 0,
    'spaced-comment': 0,
    'global-require': 0,
    'import/no-extraneous-dependencies': 0,
    'import/extensions': 0,
    'jsx-a11y/href-no-hash': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
    'unicorn/catch-error-name': ['error', { name: 'err' }],
    'unicorn/explicit-length-check': 'error',
    'unicorn/filename-case': ['error', { case: 'camelCase' }],
    'unicorn/no-abusive-eslint-disable': 'error',
    'unicorn/throw-new-error': 'error',
    'unicorn/number-literal-case': 'error',
    'unicorn/escape-case': 'error',
    'unicorn/no-array-instanceof': 'error',
    'unicorn/no-new-buffer': 'error',
    'unicorn/no-hex-escape': 'error',
    'unicorn/custom-error-definition': 'off',
    'unicorn/prefer-starts-ends-with': 'error',
    'unicorn/prefer-type-error': 'error',
    'unicorn/no-fn-reference-in-iterator': 'off',
    'unicorn/import-index': 'error',
    'unicorn/new-for-builtins': 'error',
    'unicorn/regex-shorthand': 'error',
    'unicorn/prefer-spread': 'error',
    'unicorn/error-message': 'error',
    'unicorn/no-unsafe-regex': 'off',
    'unicorn/prefer-add-event-listener': 'error'
  },
  plugins: ['unicorn', 'sonarjs']
}
