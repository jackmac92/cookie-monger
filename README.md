# Cookie Monger

## Installing the native host

```shell
deno run -A --unstable https://gitlab.com/jackmac92/install-deno-native-host/-/raw/master/mod.ts install https://gitlab.com/jackmac92/cookie-monger/-/raw/master/src/native-host/mod.ts --autoConfig --denoFlags='-A --unstable'
```
